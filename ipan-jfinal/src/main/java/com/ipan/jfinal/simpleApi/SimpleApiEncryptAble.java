package com.ipan.jfinal.simpleApi;

/**
 * 加密解密定义
 * 
 * @author iPan
 * @date 2022-07-01
 */
public interface SimpleApiEncryptAble {

	/**
	 * 内容加密
	 */
	public String encrypt(String input, String key);
	
	/**
	 * 内容解密
	 */
	public String decrypt(String input, String key);
	
}
