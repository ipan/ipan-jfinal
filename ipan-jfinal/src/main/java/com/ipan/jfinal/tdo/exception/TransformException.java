package com.ipan.jfinal.tdo.exception;

/**
 * 转换异常
 * 
 * @author iPan
 * @date 2022-01-26
 */
public class TransformException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private Integer rowIndex = -1; // 目标文件行下标
	private Integer cellIndex = -1; // 目标文件列下标
	
	public TransformException(Integer rowIndex, Integer cellIndex, String message) {
		super(message);
		this.rowIndex = rowIndex;
		this.cellIndex = cellIndex;
	}
	public TransformException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	public TransformException(String message, Throwable cause) {
		super(message, cause);
	}
	public TransformException(String message) {
		super(message);
	}
	public TransformException(Throwable cause) {
		super(cause);
	}
	
	public Integer getRowIndex() {
		return rowIndex;
	}
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
	
	public Integer getCellIndex() {
		return cellIndex;
	}
	public void setCellIndex(Integer cellIndex) {
		this.cellIndex = cellIndex;
	}

}
