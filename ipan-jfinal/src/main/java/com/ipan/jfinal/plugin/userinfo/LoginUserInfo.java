package com.ipan.jfinal.plugin.userinfo;

import java.util.List;

/**
 * 登录用户信息
 * 该类根据项目情况做修改！
 * 
 * @author iPan
 * @version 2018-04-06
 */
public class LoginUserInfo implements UserInfo {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8691433256828483575L;

	/** 用户ID */
	private java.io.Serializable id;

	/** 用户名 */
	private String userName;

	/** 昵称 */
	private String nickName;

	/** 密码 */
//	private String password;
	
	/** 人员ID */
	private Integer personId;

	/** 人员姓名 */
	private String personName;

	/** 角色编码列表 */
	private List<String> roles;
	
	private String head;
	
	public LoginUserInfo() {}
	
	public LoginUserInfo(java.io.Serializable id, String userName, String nickName, Integer personId, String personName, List<String> roles) {
		this.id = id;
		this.userName = userName;
		this.nickName = nickName;
		this.personId = personId;
		this.personName = personName;
		this.roles = roles;
	}

	@Override
	public java.io.Serializable getId() {
		return id;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public List<String> getRoles() {
		return roles;
	}

	@Override
	public String getNickName() {
		return nickName;
	}

//	public String getPassword() {
//		return password;
//	}

	public Integer getPersonId() {
		return personId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

//	public void setPassword(String password) {
//		this.password = password;
//	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

}
