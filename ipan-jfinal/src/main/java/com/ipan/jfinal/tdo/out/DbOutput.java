package com.ipan.jfinal.tdo.out;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.alibaba.excel.util.StringUtils;
import com.ipan.jfinal.tdo.config.Tpl;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Model;

/**
 * 输出到数据库
 * 
 * @author iPan
 * @date 2023-05-02
 */
public class DbOutput extends BaseOutput {
	
	/** 目标JavaBean */
	protected Class beanClass;
	
	public Class getBeanClass() {
		return beanClass;
	}

	public void setBeanClass(Class beanClass) {
		this.beanClass = beanClass;
	}

	public DbOutput(Tpl tpl, List data, Class beanClass) {
		super(tpl, data);
		this.beanClass = beanClass;
	}

	@Override
	public void execute() {
		if (tpl == null || data == null || data.size() < 1) return ;
		Db.tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				for (int i=0, len=data.size(); i<len; i++) {
					Object item = data.get(i);
					String errMsg = validator.validate(item, tpl.getKeyType());
					if (StringUtils.isNotBlank(errMsg)) { // 格式校验
						throw new java.lang.IllegalArgumentException(errMsg);
					}
					Object beanObj = rowToJavaBean(tpl, (Map) item, i+1, beanClass);
					((Model) beanObj).save(); // 特殊SQL执行、分Update与Insert的情况，自己重载execute方法；
				}
				return true;
			}
		});
	}
	
}
