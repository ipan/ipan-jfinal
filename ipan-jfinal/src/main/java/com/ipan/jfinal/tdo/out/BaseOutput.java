package com.ipan.jfinal.tdo.out;

import java.util.List;
import java.util.Map;

import com.ipan.jfinal.tdo.OutputKits;
import com.ipan.jfinal.tdo.config.Tpl;

/**
 * 同步输出
 * 
 * 格式一
 * List<Map<Integer, String>>，依赖EasyExcel；
 * 
 * 格式二
 * List<Map<String, Object>>，Json字符串的场景；
 * 
 * @author iPan
 * @date 2023-05-02
 */
public abstract class BaseOutput {
	/** 模板 */
	protected Tpl tpl;
	/** 数据 */
	protected List<?> data;
	/** 数据格式校验 */
	protected TdoValidator validator = TdoValidator.DEFAULT_VALIDATOR;
	
	public Tpl getTpl() {
		return tpl;
	}

	public void setTpl(Tpl tpl) {
		this.tpl = tpl;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}
	
	public TdoValidator getValidator() {
		return validator;
	}

	public void setValidator(TdoValidator validator) {
		this.validator = validator;
	}

	public BaseOutput(Tpl tpl, List<?> data) {
		this.tpl = tpl;
		this.data = data;
	}

	public abstract void execute();
	
	protected <E> E rowToJavaBean(Tpl tpl, Map row, int rowIndex, Class<E> beanClass) {
		E bean = OutputKits.rowToJavaBean(tpl, row, rowIndex, beanClass);
		return bean;
	}
	
}
