package com.ipan.jfinal.validator;

import com.ipan.jfinal.controller.ResultDefineManager;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * ajax 登录参数验证
 * 
 * @author iPan
 * @version 2018-04-05
 */
public class LoginValidator extends Validator {
	
	protected String k_username = "username";
	protected String k_password = "password";
	protected String k_validate_code = "validateCode";
	protected String k_error_key = "msg";
	protected String k_code_key = "code";
	
	public LoginValidator() {}

	protected void validate(Controller c) {
		setShortCircuit(true); // 短路校验

		validateRequired(k_username, k_error_key, "用户名不能为空");
		validateRequired(k_password, k_error_key, "密码不能为空");
		validateCaptcha(k_validate_code, k_error_key, "验证码不正确"); // 验证码验证成功后会删除服务端缓存中的验证码，并要求客户端删除cookie；
	}

	protected void handleError(Controller c) {
		c.setAttr(k_code_key, ResultDefineManager.me().getFail().getLeft()); // json状态码
		c.renderJson();
	}
	
}
