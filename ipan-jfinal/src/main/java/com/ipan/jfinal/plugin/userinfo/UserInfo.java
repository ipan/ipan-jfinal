package com.ipan.jfinal.plugin.userinfo;

import java.util.List;

/**
 * 通用登录用户信息接口
 * 
 * @author iPan
 * @version 2018-04-06
 */
public interface UserInfo extends java.io.Serializable {
	
	public java.io.Serializable getId();
	
	public String getUserName();
	
	public String getNickName();

	public List<String> getRoles();

}