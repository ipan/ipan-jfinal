package com.ipan.jfinal.template;

import javax.servlet.http.HttpServletRequest;

import com.ipan.jfinal.plugin.userinfo.UserInfoManager;
import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.Expr;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.ParseException;
import com.jfinal.template.stat.Scope;

/**
 * JFinal模板权限控制指令
 * 
 * 示例：#hasPermission("manager", "admin:dept:add")
 * 
 * @author iPan
 * @date 2021-11-10
 */
public class HasPermissionDirective extends Directive {
	
	private String[] permissions = null;
	private Object mark = null;

	protected boolean hasPermission(Scope scope) {
		HttpServletRequest request = (HttpServletRequest) scope.getRootData().get("request");
		String markStr = null;
		if (mark instanceof com.jfinal.template.expr.ast.Field) {
			markStr = (String) ((com.jfinal.template.expr.ast.Field)mark).eval(scope);
		} else {
			markStr = mark.toString();
		}
		return UserInfoManager.hasPermission(request, markStr, permissions);
	}
	
	@Override
	public void exec(Env env, Scope scope, Writer writer) {
		if (hasPermission(scope)) {
			stat.exec(env, scope, writer);
		}
	}
	
	@Override
	public void setExprList(ExprList exprList) {
		this.exprList = exprList;
		int paraNum = exprList.length();
		if (paraNum < 2) {
			throw new ParseException("#permission 参数异常", location);
		}
		Expr[] arr = exprList.getExprArray();
		this.permissions = new String[arr.length - 1];
		for (int i=1; i<arr.length; ++i) {
			this.permissions[i-1] = arr[i].toString();
		}
		this.mark = arr[0];
	}

	@Override
	public boolean hasEnd() {
		return true;
	}

}
