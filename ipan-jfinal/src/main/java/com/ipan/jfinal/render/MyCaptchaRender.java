package com.ipan.jfinal.render;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.QuadCurve2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.imageio.ImageIO;

import com.ipan.kits.security.HexUtil;
import com.ipan.kits.text.EncodeUtil;
import com.jfinal.captcha.Captcha;
import com.jfinal.captcha.CaptchaManager;
import com.jfinal.captcha.ICaptchaCache;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Kv;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;
import com.jfinal.render.Render;
import com.jfinal.render.RenderException;

/**
 * 类似JWT的方式保存key
 * 
 * 前后端分离开发，使用cookie非常麻烦，所以将key保存在http head；图片内容以base64方式提供；
 * 
 * @author iPan
 * @date 2021-12-02
 */
public class MyCaptchaRender extends Render {

	protected static final String contentType = "application/json; charset=" + getEncoding();
	protected static final String contentTypeForIE = "text/html; charset=" + getEncoding();
	protected boolean forIE = false;
	protected static String captchaName = "Captcha";
	protected static final Random random = new Random(System.nanoTime());
	
	// 默认的验证码大小
	protected static final int WIDTH = 108, HEIGHT = 40;
	// 验证码随机字符数组
	protected static final char[] charArray = "123456789ABCDEFGHJKMNPQRSTUVWXY".toCharArray();
	// 验证码字体
	protected static final Font[] RANDOM_FONT = new Font[] {
		new Font(Font.DIALOG, Font.BOLD, 33),
		new Font(Font.DIALOG_INPUT, Font.BOLD, 34),
		new Font(Font.SERIF, Font.BOLD, 33),
		new Font(Font.SANS_SERIF, Font.BOLD, 34),
		new Font(Font.MONOSPACED, Font.BOLD, 34)
	};
	
	/**
	 * 设置 captchaName
	 */
	public static void setCaptchaName(String captchaName) {
		if (StrKit.isBlank(captchaName)) {
			throw new IllegalArgumentException("captchaName can not be blank.");
		}
		MyCaptchaRender.captchaName = captchaName;
	}
	
	public MyCaptchaRender forIE() {
		forIE = true;
		return this;
	}
	
	/**
	 * 生成验证码
	 */
	public void render() {
		Captcha captcha = createCaptcha();
		CaptchaManager.me().getCaptchaCache().put(captcha);
		response.setHeader(captchaName, HexUtil.encodeHexStr(captcha.getKey()));
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-cache");
		response.setDateHeader("Expires", 0);
//		response.setContentType("image/jpeg");
		response.setContentType(forIE ? contentTypeForIE : contentType);
		
		PrintWriter writer = null;
		ByteArrayOutputStream bos = null;
		try {
			BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
			drawGraphic(captcha.getValue(), image);
			
			writer = response.getWriter(); // 客户端不能通过二进制方式接收，需要通过ajax方式接收字符串；
//			ImageIO.write(image, "jpeg", sos);
			bos = new ByteArrayOutputStream();
			ImageIO.write(image, "jpeg", bos);
			byte[] imageByte = bos.toByteArray();
			String base64Image = "data:image/jpeg;base64," + EncodeUtil.encodeBase64(imageByte);
			Kv kv = Kv.by("code", "0").set("msg", "操作成功").set("data", base64Image);
			writer.println(JsonKit.toJson(kv));
			writer.flush();
		} catch (Exception e) {
			throw new RenderException(e);
		} finally {
			if (bos != null) {
				try {bos.close();} catch (IOException e) {LogKit.logNothing(e);}
			}
			if (writer != null) {
				try {writer.close();} catch (Exception e) {LogKit.logNothing(e);}
			}
		}
	}
	
	protected Captcha createCaptcha() {
		String captchaKey = request.getHeader(captchaName); // 前端有可能没传
		if (StrKit.isBlank(captchaKey)) {
			captchaKey = StrKit.getRandomUUID();
		}
		return new Captcha(captchaKey, getRandomString(), Captcha.DEFAULT_EXPIRE_TIME);
	}
	
	protected String getRandomString() {
		char[] randomChars = new char[4];
		for (int i=0; i<randomChars.length; i++) {
			randomChars[i] = charArray[random.nextInt(charArray.length)];
		}
		return String.valueOf(randomChars);
	}
	
	protected void drawGraphic(String randomString, BufferedImage image){
		// 获取图形上下文
		Graphics2D g = image.createGraphics();
		
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		// 图形抗锯齿
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		// 字体抗锯齿
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		// 设定背景色
		g.setColor(getRandColor(210, 250));
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		//绘制小字符背景
		Color color = null;
		for(int i = 0; i < 20; i++){
			color = getRandColor(120, 200);
			g.setColor(color);
			String rand = String.valueOf(charArray[random.nextInt(charArray.length)]);
			g.drawString(rand, random.nextInt(WIDTH), random.nextInt(HEIGHT));
			color = null;
		}
		
		//设定字体
		g.setFont(RANDOM_FONT[random.nextInt(RANDOM_FONT.length)]);
		// 绘制验证码
		for (int i = 0; i < randomString.length(); i++){
			//旋转度数 最好小于45度
			int degree = random.nextInt(28);
			if (i % 2 == 0) {
				degree = degree * (-1);
			}
			//定义坐标
			int x = 22 * i, y = 21;
			//旋转区域
			g.rotate(Math.toRadians(degree), x, y);
			//设定字体颜色
			color = getRandColor(20, 130);
			g.setColor(color);
			//将认证码显示到图象中
			g.drawString(String.valueOf(randomString.charAt(i)), x + 8, y + 10);
			//旋转之后，必须旋转回来
			g.rotate(-Math.toRadians(degree), x, y);
		}
		//图片中间曲线，使用上面缓存的color
		g.setColor(color);
		//width是线宽,float型
		BasicStroke bs = new BasicStroke(3);
		g.setStroke(bs);
		//画出曲线
		QuadCurve2D.Double curve = new QuadCurve2D.Double(0d, random.nextInt(HEIGHT - 8) + 4, WIDTH / 2, HEIGHT / 2, WIDTH, random.nextInt(HEIGHT - 8) + 4);
		g.draw(curve);
		// 销毁图像
		g.dispose();
	}
	
	/*
	 * 给定范围获得随机颜色
	 */
	protected Color getRandColor(int fc, int bc) {
		Random random = new Random();
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}
	
	/**
	 * 校验用户输入的验证码是否正确
	 * @param controller 控制器
	 * @param userInputString 用户输入的字符串
	 * @return 验证通过返回 true, 否则返回 false
	 */
	public static boolean validate(Controller controller, String userInputString) {
		String captchaKey = controller.getRequest().getHeader(captchaName);
		if (captchaKey == null || captchaKey.length() < 1) {
			return false;
		}
		String decodeKey = null;
		try {
			decodeKey = HexUtil.decodeHexStr(captchaKey);
		} catch (Exception e) {
			return false;
		}
		return validate(decodeKey, userInputString);
	}
	
	/**
	 * 校验用户输入的验证码是否正确
	 * @param captchaKey 验证码 key
	 * @param userInputString 用户输入的字符串
	 * @return 验证通过返回 true, 否则返回 false
	 */
	public static boolean validate(String captchaKey, String userInputString) {
		ICaptchaCache captchaCache = CaptchaManager.me().getCaptchaCache();
		Captcha captcha = captchaCache.get(captchaKey);
		boolean ret = false;
		if (captcha != null && captcha.notExpired() && captcha.getValue().equalsIgnoreCase(userInputString)) {
			ret = true;
		}
		if (captcha != null) captchaCache.remove(captcha.getKey()); // 不论校验成功、失败，都即时删除原数据；失败后需要重新刷新验证码；
		return ret;
	}
	
}
