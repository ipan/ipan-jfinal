package com.ipan.jfinal.plugin.activerecord;

import java.util.Set;
import java.util.TreeSet;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.generator.MetaBuilder;

/**
 * 根据指定表名列表来生成文件
 * 
 * @author iPan
 * @date 2021-12-21
 */
public class IncludedMetaBuilder extends MetaBuilder {
	
	protected Set<String> includedTables = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	
	public void addIncludedTable(String... tables) {
		if (tables != null) {
			for (String table : tables) {
				this.includedTables.add(table.trim());
			}
		}
	}

	public IncludedMetaBuilder(DataSource dataSource) {
		super(dataSource);
		this.tableSkip = (tableName) -> {
			return !includedTables.contains(tableName); // 只接收includedTables定义的表
		};
		typeMapping.addMapping("java.sql.Array", "java.lang.String[]"); // 增加数组映射，默认使用String[]。
	}
	
}
