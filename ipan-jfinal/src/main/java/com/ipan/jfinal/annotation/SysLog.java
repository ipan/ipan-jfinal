package com.ipan.jfinal.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ipan.jfinal.enums.BusinessType;

/**
 * 访问日志注解
 * 
 * 消息内容依赖JFinal的模板，可以使用_user.xxx来访问登录用户信息；
 * 
 * @author iPan
 * @version 2018-05-13
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SysLog {
	
	/**
	 * 消息模式
	 */
	public static enum Pattern {
		FRONT, AFTER, FINISH, ALL;
	}
	
	/**
	 * 消息类型
	 */
	public static enum Type {
		
		LOGIN("登入日志"), LOGOUT("登出日志"), ACTION("操作日志");
		
		private Type(String title) {
			this.title = title;
		}
		
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		private String title;
	}
	
	/** 前置消息日志（支持JF模板） */
	String preMessage() default "";
	
	/** 后置消息日志（支持JF模板） */
	String postMessage() default "";
	
	/** 完成消息日志（支持JF模板） */
	String finishMessage() default "";
	
	/** 消息处理模式（默认后置通知） */
	Pattern pattern() default Pattern.FINISH;
	
	/** 日志类型(默认操作日志) */
	Type type() default Type.ACTION;
	
	/**
     * 是否保存请求的参数
     */
    public boolean isSaveRequestData() default true;
    
    /**
     * 是否保存返回的参数（一般对操作保存，对查询列表不保存。）
     */
    public boolean isSaveResponseData() default false; // 默认不要保存 2023-11-03
    
    /**
     * 业务操作类型
     */
    public BusinessType businessType() default BusinessType.OTHER;
	
}
