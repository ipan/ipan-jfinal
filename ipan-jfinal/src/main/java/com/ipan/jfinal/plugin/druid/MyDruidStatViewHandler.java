package com.ipan.jfinal.plugin.druid;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.util.Utils;
import com.jfinal.handler.Handler;
import com.jfinal.kit.HandlerKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.druid.IDruidStatViewAuth;

/**
 * 自定义druid访问策略
 * 
 * 1 去除druid监控系统底部的广告
 * 2 当前登录用户授权使用当前系统用户
 * 
 * 修改自druid-1.2.8
 * 
 * @author iPan
 * @date 2021-12-01
 */
public class MyDruidStatViewHandler extends Handler {
	private Logger logger = LoggerFactory.getLogger(getClass());
	private IDruidStatViewAuth auth;
	private String visitPath = "/druid";
	private final static String COMMONJS_VISITPATH = "/druid/js/common.js";
	private StatViewServlet servlet = new JFinalStatViewServlet();
	
	public MyDruidStatViewHandler(String visitPath) {
		this(visitPath,
			new IDruidStatViewAuth(){
				public boolean isPermitted(HttpServletRequest request) {
					return true;
				}
			});
	}
	
	public MyDruidStatViewHandler(String visitPath , IDruidStatViewAuth druidStatViewAuth) {
		if (StrKit.isBlank(visitPath))
			throw new IllegalArgumentException("visitPath can not be blank");
		if (druidStatViewAuth == null)
			throw new IllegalArgumentException("druidStatViewAuth can not be null");
		
		visitPath = visitPath.trim();
		if (! visitPath.startsWith("/"))
			visitPath = "/" + visitPath;
		this.visitPath = visitPath;
		this.auth = druidStatViewAuth;
	}
	
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		if(target.endsWith(COMMONJS_VISITPATH)) {
			isHandled[0] = true;
			try {
				String text = Utils.readFromResource("support/http/resources/js/common.js");
				text = text.replaceAll("<a.*?banner\"></a><br/>", "");
                text = text.replaceAll("powered.*?shrek.wang</a>", "");
				response.setContentType("text/javascript;charset=utf-8");
                response.getWriter().write(text);
			} catch (IOException e) {
				logger.error("去除druid底部广告失败", e);
			}
			return ;
			
		} else if (target.startsWith(visitPath)) {
			isHandled[0] = true;
			// 支持 context path
			String ctx = request.getContextPath();
			if (ctx != null && !"".equals(ctx) && !"/".equals(ctx)) {
				target = ctx + target;
			}
			if (target.equals(visitPath) && !target.endsWith("/index.html")) {
				HandlerKit.redirect(target += "/index.html", request, response, isHandled);
				return ;
			}
			
			try {
				servlet.service(request, response);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			
		} else {
			next.handle(target, request, response, isHandled);
		}
	}
	
	class JFinalStatViewServlet extends StatViewServlet {
		
		private static final long serialVersionUID = 1L;

		public boolean isPermittedRequest(HttpServletRequest request) {
			return auth.isPermitted(request);
		}
		
		public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        String contextPath = request.getContextPath();
	        // String servletPath = request.getServletPath();
	        String requestURI = request.getRequestURI();

	        response.setCharacterEncoding("utf-8");

	        if (contextPath == null) { // root context
	            contextPath = "";
	        }
	        // String uri = contextPath + servletPath;
	        // String path = requestURI.substring(contextPath.length() + servletPath.length());
	        int index = contextPath.length() + visitPath.length();
	        String uri = requestURI.substring(0, index);
	        String path = requestURI.substring(index);

	        if (!isPermittedRequest(request)) {
	            path = "/nopermit.html";
	            returnResourceFile(path, uri, response);
	            return;
	        }

	        if ("/submitLogin".equals(path)) {
	            String usernameParam = request.getParameter(PARAM_NAME_USERNAME);
//	            String passwordParam = request.getParameter(PARAM_NAME_PASSWORD);
//	            if (username.equals(usernameParam) && password.equals(passwordParam)) {
	            if (handler.checkLoginParam(request)) {
	                request.getSession().setAttribute(SESSION_USER_KEY, usernameParam);
	                response.getWriter().print("success");
	            } else {
	                response.getWriter().print("error");
	            }
	            return;
	        }

	        if (isRequireAuth() //
	            && !ContainsUser(request)//
	            && !("/login.html".equals(path) //
	                 || path.startsWith("/css")//
	                 || path.startsWith("/js") //
	            || path.startsWith("/img"))) {
	            if (contextPath == null || contextPath.equals("") || contextPath.equals("/")) {
	                response.sendRedirect("/druid/login.html");
	            } else {
	                if ("".equals(path)) {
	                    response.sendRedirect("druid/login.html");
	                } else {
	                    response.sendRedirect("login.html");
	                }
	            }
	            return;
	        }

	        if ("".equals(path)) {
	            if (contextPath == null || contextPath.equals("") || contextPath.equals("/")) {
	                response.sendRedirect("/druid/index.html");
	            } else {
	                response.sendRedirect("druid/index.html");
	            }
	            return;
	        }

	        if ("/".equals(path)) {
	            response.sendRedirect("index.html");
	            return;
	        }

	        if (path.indexOf(".json") >= 0) {
	            String fullUrl = path;
	            if (request.getQueryString() != null && request.getQueryString().length() > 0) {
	                fullUrl += "?" + request.getQueryString();
	            }
	            response.getWriter().print(process(fullUrl));
	            return;
	        }

	        // find file in resources path
	        returnResourceFile(path, uri, response);
	    }
	}

}
