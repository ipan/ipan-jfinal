package com.ipan.jfinal.tdo.config;

/**
 * 映射配置
 * 
 * @author iPan
 * @date 2023-05-02
 */
public class Mp {
	/** 目标字段名称 */
	private String toVal = null;
	/** 目标字段标题 */
	private String toLabel = null;
	/** 源字段序列 */
	private String srcVal = null; // 文件使用0、1、2数字，json字符串使用字段名；
	/** 源字段标题 */
	private String srcLabel = null;
	/** 格式转换定义 */
	private String format = null;
	
	public Mp() {}
	
	public Mp(String toVal, String toLabel, String srcVal, String srcLabel, String format) {
		this.toVal = toVal;
		this.toLabel = toLabel;
		this.srcVal = srcVal;
		this.srcLabel = srcLabel;
		this.format = format;
	}

	public String getToVal() {
		return toVal;
	}
	public void setToVal(String toVal) {
		this.toVal = toVal;
	}
	public String getToLabel() {
		return toLabel;
	}
	public void setToLabel(String toLabel) {
		this.toLabel = toLabel;
	}
	public String getSrcVal() {
		return srcVal;
	}
	public void setSrcVal(String srcVal) {
		this.srcVal = srcVal;
	}
	public String getSrcLabel() {
		return srcLabel;
	}
	public void setSrcLabel(String srcLabel) {
		this.srcLabel = srcLabel;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}

}
