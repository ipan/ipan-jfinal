package com.ipan.jfinal.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jfinal.json.FastJson;

/**
 * 自定义FastJson
 * 对JavaBean的Date类型不统一使用具体格式，而是交给FastJson的注解来控制！
 * 对其他对象的Date类型统一处理；
 * 
 * @author iPan
 * @version 2019-01-10
 */
public class MyFastJson extends FastJson {
	
	private SerializerFeature[] defaultSerializerFeature = null;
	
	public MyFastJson() {}
	
	public MyFastJson(SerializerFeature[] defaultSerializerFeature) {
		this.defaultSerializerFeature = defaultSerializerFeature;
	}
	
	@Override
	public String toJson(Object object) {
//		if (object instanceof IBean) {
//			return JSON.toJSONString(object);
//		}
		// 优先使用对象级的属性 datePattern, 然后才是全局性的 defaultDatePattern
//		String dp = datePattern != null ? datePattern : getDefaultDatePattern();
//		if (dp == null) {
//			return JSON.toJSONString(object);
//		} else {
//			return JSON.toJSONStringWithDateFormat(object, dp, SerializerFeature.WriteDateUseDateFormat);	// return JSON.toJSONString(object, SerializerFeature.WriteDateUseDateFormat);
//		}
		// 彻底放弃全局的日期格式设定，只使用FastJson的默认实现；
		// 日期时间的格式自定义使用FastJson的注解，不仅对Model需要对Map包装的Model也需要，所以干脆使用默认实现更合适！2019-09-28
		return (defaultSerializerFeature == null) ? JSON.toJSONString(object) : JSON.toJSONString(object, defaultSerializerFeature);
	}
	
}
