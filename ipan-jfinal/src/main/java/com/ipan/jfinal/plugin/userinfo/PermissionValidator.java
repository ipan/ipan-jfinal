package com.ipan.jfinal.plugin.userinfo;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * 权限验证器
 * 
 * @author iPan
 * @date 2021-11-10
 */
public interface PermissionValidator {

	/**
	 * 是否有权限
	 * 前后端使用一个会话存储的场景
	 */
	public boolean hasPermission(HttpServletRequest request, String... permissions);
	
	/**
	 * 是否有权限
	 * 前后端使用两个会话存储的场景，使用mark区分；
	 */
	public boolean hasPermission(HttpServletRequest request, String mark, String... permissions);
	
}
