package com.ipan.jfinal.plugin.userinfo;

/**
 * 多端配置
 * 
 * @author iPan
 * @version 2019-07-02
 */
public class MultiLoginPlatform {
	/** 平台 */
	private String name;
	/** 是否支持多人同时登录 */
	private boolean multiLogin;
	/** 超时时间（秒） */
	private int maxAge;
	
	public MultiLoginPlatform() {}
	
	public MultiLoginPlatform(String name, boolean multiLogin, int maxAge) {
		this.name = name;
		this.multiLogin = multiLogin;
		this.maxAge = maxAge;
	}
	public String getName() {
		return name;
	}
	public boolean isMultiLogin() {
		return multiLogin;
	}
	public int getMaxAge() {
		return maxAge;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMultiLogin(boolean multiLogin) {
		this.multiLogin = multiLogin;
	}
	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}
	@Override
	public String toString() {
		return "MultiLoginCfg [name=" + name + ", multiLogin=" + multiLogin + ", maxAge=" + maxAge + "]";
	}
}
