package com.ipan.jfinal.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限验证
 * 
 * @author iPan
 * @date 2021-11-10
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiresPermissions {

	/** 权限（多个权限是or的关系） */
	String[] value() default "";
	
	/** 选项（true: 存在该权限，默认； false: 不存在该权限；） */
	boolean existed() default true;
}
