package com.ipan.jfinal.enums;

/**
 * 业务操作类型
 * 
 * 业务操作类型跟数据字典表配对，可以配置状态显示的样式；
 * 
 * @author iPan
 * @date 2021-11-15
 */
public enum BusinessType {
	OTHER("其它", "99"),
	INSERT("新增", "1"),
	UPDATE("修改", "2"),
	DELETE("删除", "3"),
	VIEW("查询", "4"),
	GRANT("授权", "5"),
	EXPORT("导出", "6"),
	IMPORT("导入", "7"),
	FORCE("强退", "8"),
	GENERATE("生成代码", "9"),
	CLEAN("清空数据", "10"),
	KICKED("被踢", "11");

	private BusinessType(String title, String code) {
		this.title = title;
		this.code = code;
	}

	public String getTitle() {
		return title;
	}
	
	public String getCode() {
		return code;
	}

	private String title;
	private String code;
}
