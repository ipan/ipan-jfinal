package com.ipan.jfinal.plugin.userinfo;

import java.util.List;

/**
 * 通用登录用户信息
 * 
 * @author iPan
 * @version 2018-04-06
 */
public class DefaultUserInfo implements UserInfo {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 143118969184731034L;
	
	private java.io.Serializable id;
	private String userName;
	private String nickName;
	private List<String> roles;
	
	public DefaultUserInfo() {}

	public DefaultUserInfo(int id, String userName, String nickName, List<String> roles) {
		this.id = id;
		this.userName = userName;
		this.nickName = nickName;
		this.roles = roles;
	}

	@Override
	public java.io.Serializable getId() {
		return id;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public List<String> getRoles() {
		return roles;
	}

	@Override
	public String getNickName() {
		return nickName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "DefaultUserInfo [id=" + id + ", userName=" + userName + ", nickName=" + nickName + ", roles=" + roles + "]";
	}

}
