package com.ipan.jfinal.plugin.userinfo;

import java.util.List;

import com.ipan.kits.mapper.FastJsonMapper;

/**
 * 多端配置
 * 
 * @author iPan
 * @version 2019-07-02
 */
public class MultiLoginCfg {
	/** 不允许多人同时登录，是否需要提醒用户（true要通知、false不通知；） */
	private boolean remind;
	/** 两次请求间隔多久需刷入数据库（秒） */
	private int interval = 60; // 将会话最后访问时间刷入数据库
	/** 多平台的配置 */
	private List<MultiLoginPlatform> platforms = null;
	
	public boolean isRemind() {
		return remind;
	}
	public void setRemind(boolean remind) {
		this.remind = remind;
	}
	public List<MultiLoginPlatform> getPlatforms() {
		return platforms;
	}
	public void setPlatforms(List<MultiLoginPlatform> platforms) {
		this.platforms = platforms;
	}
	
	public int getPlatformCount() {
		return (platforms != null) ? platforms.size() : 0;
	}
	
	public MultiLoginPlatform getPlatform(String name) {
		if (platforms == null || name == null) {
			return null;
		}
		MultiLoginPlatform pf = null;
		for (MultiLoginPlatform item : platforms) {
			if (name.equals(item.getName())) {
				pf = item;
				break;
			}
		}
		return pf;
	}
	
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
	
	@Override
	public String toString() {
		return FastJsonMapper.me().toJson(this);
	}
	
}
