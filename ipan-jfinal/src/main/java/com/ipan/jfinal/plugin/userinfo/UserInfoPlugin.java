package com.ipan.jfinal.plugin.userinfo;

import com.jfinal.plugin.IPlugin;

/**
 * 用户信息存储插件
 * 
 * @author iPan
 * @version 2018-05-13
 */
public class UserInfoPlugin implements IPlugin {
	
	private UserInfoHandler handler = null;
//	private IConvertUserInfo converter = null;
	private RoleValidator roleValidator = null;
	private PermissionValidator permissionValidator;

	public UserInfoPlugin(UserInfoHandler handler, RoleValidator roleValidator, PermissionValidator pv) {
		this.handler = handler;
//		this.converter = converter;
		this.roleValidator = roleValidator;
		this.permissionValidator = pv;
	}

	@Override
	public boolean start() {
		UserInfoManager.init(handler, roleValidator, permissionValidator);
		return true;
	}

	@Override
	public boolean stop() {
		return true;
	}

}
