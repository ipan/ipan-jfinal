package com.ipan.jfinal.template;

/**
 * 常用模板引擎常量
 * 
 * @author iPan
 * @date 2021-11-23
 */
public interface EnjoyConstant{

	/** 动态where 键名 */
	public static final String WHERE_PARAM = "whereParam";
	
	/** 动态order by 键名 */
	public static final String ORDER_BY_PARAM = "orderByParam";
	
	/** 动态in 键名 */
	public static final String IN_PARAM = "inParam";
	
	/** 数据权限SQL 键名 */
	public static final String DATASCOPE_PARAM = "dataScope";
}
