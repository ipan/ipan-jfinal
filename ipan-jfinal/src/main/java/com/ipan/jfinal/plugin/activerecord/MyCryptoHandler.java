package com.ipan.jfinal.plugin.activerecord;

/**
 * 
 * 对称加密解密处理器
 * 
 * 私钥交给子类保管；
 * 
 * @author iPan
 * @date 2021-12-07
 */
public interface MyCryptoHandler {

	/**
	 * 加密
	 * 
	 * @param input 加密字符串
	 * @param key 秘钥
	 * @return 密文
	 */
	public String encrypt(String input);
	
	/**
	 * 解密
	 * 
	 * @param input 解密字符串
	 * @param key 秘钥
	 * @return 明文
	 */
	public String decrypt(String input);
	
}
