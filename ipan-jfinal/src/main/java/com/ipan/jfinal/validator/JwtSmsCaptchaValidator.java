package com.ipan.jfinal.validator;

import com.ipan.jfinal.controller.ResultDefineManager;
import com.ipan.jfinal.render.MyCaptchaRender;
import com.ipan.kits.security.HexUtil;
import com.jfinal.captcha.Captcha;
import com.jfinal.captcha.CaptchaManager;
import com.jfinal.captcha.ICaptchaCache;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * 短信验证码+图形验证码_通用JWT校验器
 * 
 * 一般用不到，一般是用图形验证码+手机号获取短信验证码，用短信验证码+手机号直接登录；
 * 
 * @author iPan
 * @date 2023-02-01
 * @deprecated 用不到
 */
public class JwtSmsCaptchaValidator extends Validator {
	
	protected String k_sms_key = "smsKey";
	protected String k_sms_value = "smsValue";
	protected String k_captcha_key = "Captcha"; // 图形验证码Key
	protected String k_captcha_value = "validateCode"; // 图形验证码Value
	protected String k_error_key = "msg";
	protected String k_code_key = "code";

	public JwtSmsCaptchaValidator() {}
	
	@Override
	protected void validate(Controller c) {
		setShortCircuit(true); // 短路校验
		
		// 表单提交参数
		String captchaValue = c.getPara(k_captcha_value); // 图形验证码Value
		String smsKey = c.getPara(k_sms_key); // 短信验证码Key
		String smsValue = c.getPara(k_sms_value); // 短信验证码Value
		String tmpCaptchaKey = c.getRequest().getHeader(k_captcha_key);
		if (tmpCaptchaKey == null || tmpCaptchaKey.length() < 1) {
			addError(k_error_key, "图形验证码不正确"); // 标识校验失败
			return ;
		}
		String captchaKey = null; // 图形验证码Key，简单做了HexEncode。
		try {
			captchaKey = HexUtil.decodeHexStr(tmpCaptchaKey);
		} catch (Exception e) {
			addError(k_error_key, "图形验证码不正确"); // 标识校验失败
			return ;
		}
		
		// 校验图形验证码
		boolean captchaValidate = MyCaptchaRender.validate(captchaKey, captchaValue);
		if (!captchaValidate) {
			addError(k_error_key, "图形验证码不正确"); // 标识校验失败
			return ;
		}
		
		// 校验手机验证码
		ICaptchaCache captchaCache = CaptchaManager.me().getCaptchaCache();
		Captcha captcha = captchaCache.get(smsKey);
		if (captcha != null && captcha.notExpired() && captcha.getValue().equalsIgnoreCase(smsValue)) {
			captchaCache.remove(captcha.getKey());
		} else {
			addError(k_error_key, "手机验证码不正确"); // 标识校验失败
		}
	}

	@Override
	protected void handleError(Controller c) {
		c.setAttr(k_code_key, ResultDefineManager.me().getFail().getLeft()); // json状态码
		c.renderJson();
	}

}
