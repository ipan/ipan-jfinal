package com.ipan.jfinal.controller;

import java.util.HashMap;
import java.util.Map;

import com.ipan.kits.base.type.Pair;
import com.ipan.kits.text.MoreStringUtil;

/**
 * 
 * 返回类型管理器
 * 
 * 根据具体项目不同自行继承实现，也要跟前端开发一致；
 * 
 * @author iPan
 * @date 2021-11-12
 */
public class ResultDefineManager {
	// 返回的状态码自定义
	private Map<String, Pair<String, String>> codes = new HashMap<String, Pair<String, String>>(20);
	// 返回分页表格的键名
	private Map<String, String> codeKeys = new HashMap<String, String>();
	
	private static ResultDefineManager _ME = new ResultDefineManager();
	
	public static ResultDefineManager me() {
		return _ME;
	}
	
	private ResultDefineManager() {
		init();
	}
	
	public void reInit(Initializable init) {
		codes.clear();
		codeKeys.clear();
		init.init(codes, codeKeys);
	}
	
	public void init() {
		// 返回的键值
		codeKeys.put("code", "code");
		codeKeys.put("msg", "msg");
		codeKeys.put("total", "total");
		codeKeys.put("rows", "rows");
		// 提交的键值
		codeKeys.put("pageNum", "pageNum");
		codeKeys.put("pageSize", "pageSize");
		codeKeys.put("searchValue", "searchValue");
		codeKeys.put("orderByColumn", "orderByColumn");
		codeKeys.put("isAsc", "isAsc");
		
		Pair<String, String> success = Pair.of("0", "操作成功");
		Pair<String, String> fail = Pair.of("500", "操作失败");
		Pair<String, String> warn = Pair.of("301", ""); // 警告
		Pair<String, String> notFound = Pair.of("404", "请求失败");
		Pair<String, String> unauthorized = Pair.of("401", "认证失败");
		Pair<String, String> forbidden = Pair.of("403", "权限不足");
		Pair<String, String> unLogin = Pair.of("600", "账号未登录");
		Pair<String, String> accountError = Pair.of("601", "账号或密码错误");
		Pair<String, String> timeout = Pair.of("602", "登录超时");
		Pair<String, String> dropOut = Pair.of("603", "被强退下线");
		Pair<String, String> kickDown = Pair.of("604", "被新用户登录踢下线");
		
		codes.put("success", success);
		codes.put("fail", fail);
		codes.put("warn", warn);
		codes.put("notFound", notFound);
		codes.put("unauthorized", unauthorized);
		codes.put("forbidden", forbidden);
		codes.put("unLogin", unLogin);
		codes.put("accountError", accountError);
		codes.put("timeout", timeout);
		codes.put("dropOut", dropOut);
		codes.put("kickDown", kickDown);
	}
	
	public void register(String code, String msg) {
		codes.put(code, Pair.of(code, msg));
	}
	
	public Pair<String, String> get(String code) {
		return codes.get(code);
	}
	
	public Pair<String, String> getSuccess(){
		return get("success");
	}
	public Pair<String, String> getFail() {
		return get("fail");
	}
	public Pair<String, String> getWarn() {
		return get("warn");
	}
	public Pair<String, String> getNotfound() {
		return get("notFound");
	}
	public Pair<String, String> getUnauthorized() {
		return get("unauthorized");
	}
	public Pair<String, String> getForbidden() {
		return get("forbidden");
	}
	public Pair<String, String> getUnlogin() {
		return get("unLogin");
	}
	public Pair<String, String> getAccounterror() {
		return get("accountError");
	}
	public Pair<String, String> getTimeout() {
		return get("timeout");
	}
	public Pair<String, String> getDropOut() {
		return get("dropOut");
	}
	public Pair<String, String> getKickDown() {
		return get("kickDown");
	}
	
	public String getCodeKey() {
		return MoreStringUtil.toStr(codeKeys.get("code"), "code");
	}
	public String getMsgKey() {
		return MoreStringUtil.toStr(codeKeys.get("msg"), "msg");
	}
	public String getTotalKey() {
		return MoreStringUtil.toStr(codeKeys.get("total"), "total");
	}
	public String getRowsKey() {
		return MoreStringUtil.toStr(codeKeys.get("rows"), "rows");
	}
	public String getPageNum() {
		return MoreStringUtil.toStr(codeKeys.get("pageNum"), "pageNum");
	}
	public String getPageSize() {
		return MoreStringUtil.toStr(codeKeys.get("pageSize"), "pageSize");
	}
	public String getSearchValue() {
		return MoreStringUtil.toStr(codeKeys.get("searchValue"), "searchValue");
	}
	public String getOrderByColumn() {
		return MoreStringUtil.toStr(codeKeys.get("orderByColumn"), "orderByColumn");
	}
	public String getIsAsc() {
		return MoreStringUtil.toStr(codeKeys.get("isAsc"), "isAsc");
	}
	
	public static interface Initializable {
		public void init(Map<String, Pair<String, String>> context, Map<String, String> codeKeys);
	}
	
}
