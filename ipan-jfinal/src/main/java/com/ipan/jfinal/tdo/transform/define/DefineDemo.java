package com.ipan.jfinal.tdo.transform.define;

import java.util.Map;

import com.ipan.jfinal.tdo.config.Tpl;
import com.ipan.jfinal.tdo.transform.DefineMethodRegister.DefineMethod;
import com.ipan.kits.text.MoreStringUtil;

/**
 * 自定义字段转换器示例
 * 
 * @author iPan
 * @date 2022-01-27
 */
public class DefineDemo implements DefineMethod {

	@Override
	public String name() {
		return "demo";
	}

	@Override
	public String call(Map rowMap, Object rowKey, String params, Tpl config) {
		String value = MoreStringUtil.toStr(rowMap.get(rowKey));
		return "自定义[" + value + "=" + params + "]";
	}

}
