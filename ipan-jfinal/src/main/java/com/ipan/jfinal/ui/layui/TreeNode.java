package com.ipan.jfinal.ui.layui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.JsonKit;

/**
 * Layui——Tree节点信息
 * 
 * @author iPan
 * @version 2018-03-18
 */
public class TreeNode {
	// Layui——Tree参数
	private String name = null;
	private boolean spread = false;
	private String href = null;
	private List<TreeNode> children = null;

	// 自定义参数
	private Serializable id = null;
	private String code = null;
	private String icon = null;
	
	public TreeNode() {}
	public TreeNode(Serializable id, String name) {
		this(id, name, false, null, null, null);
	}
	public TreeNode(Serializable id, String name, boolean spread) {
		this(id, name, spread, null, null, null);
	}
	public TreeNode(Serializable id,  String name, boolean spread, String href, String icon) {
		this(id, name, spread, href, icon, null);
	}
	public TreeNode(Serializable id, String name, boolean spread, String href, String icon, String code) {
		this.name = name;
		this.spread = spread;
		this.href = href;
		this.id = id;
		this.icon = icon;
		this.code = code;
	}
	
	public void addChildren(TreeNode obj) {
		if (children == null) {
			children = new ArrayList<TreeNode>();
		}
		children.add(obj);
	}

	public String getName() {
		return name;
	}

	public boolean isSpread() {
		return spread;
	}

	public String getHref() {
		return href;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public Serializable getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSpread(boolean spread) {
		this.spread = spread;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return toJson();
	}
	
	public String toJson() {
		return JsonKit.toJson(this);
	}
	
}
