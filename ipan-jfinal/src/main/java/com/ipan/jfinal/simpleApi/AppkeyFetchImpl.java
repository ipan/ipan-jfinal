package com.ipan.jfinal.simpleApi;

import java.util.HashMap;
import java.util.Map;

/**
 * 根据userid获取appkey
 * 
 * @author iPan
 * @date 2022-06-29
 */
public class AppkeyFetchImpl implements AppkeyFetchable {
	
	private String appid = null;
	private Map<String, String> map = new HashMap<>();
	
	public AppkeyFetchImpl(String appid) {
		this.appid = appid;
	}
	
	@Override
	public String getAppid() {
		return appid;
	}

	@Override
	public String getAppkey(String userid) {
		return map.get(userid);
	}

	@Override
	public void putAppkey(String userid, String appkey) {
		map.put(userid, appkey);
	}

	@Override
	public void removeAppkey(String userid) {
		map.remove(userid);
	}

	@Override
	public void initAppkey(Map<String, String> initMap) {
		map.putAll(initMap);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public int size() {
		return map.size();
	}

}
