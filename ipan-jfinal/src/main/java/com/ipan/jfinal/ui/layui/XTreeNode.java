package com.ipan.jfinal.ui.layui;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.JsonKit;

/**
 * title：string 显示的值 （必填） 
 * value：string 隐藏的值 （必填） 
 * checked：bool默认是否选中，true为选中，false与不填都为不选中 （选填） 
 * disabled：bool 是否可用，true为不可用，false与不填都为可用（选填） 
 * data： json数组 节点的子节点数组，结构与此节点一致，（必填）如果无子节点则必须为 data:[]；
 * 
 * 补充说明： 
 * checked只作用于末级节点，有子级的节点不起作用； 
 * disabled作用于非末级节点，你会感觉很奇怪；
 * 
 * @author iPan
 * @version 2018-03-26
 */
public class XTreeNode {
	private String title = null;
	private String value = null;
	private Boolean checked = false;
	private Boolean disabled = false;
	private List<XTreeNode> data = null;

	public XTreeNode() {}
	public XTreeNode(String title, String value) {
		this.title = title;
		this.value = value;
		this.data = new ArrayList<XTreeNode>();
	}
	public XTreeNode(String title, String value, List<XTreeNode> data) {
		this.title = title;
		this.value = value;
		this.data = data;
	}
	public XTreeNode(String title, String value, Boolean checked, Boolean disabled, List<XTreeNode> data) {
		this.title = title;
		this.value = value;
		this.checked = checked;
		this.disabled = disabled;
		this.data = data;
	}
	
	public void addChildren(XTreeNode obj) {
		if (data == null) {
			data = new ArrayList<XTreeNode>();
		}
		data.add(obj);
	}

	public String getTitle() {
		return title;
	}

	public String getValue() {
		return value;
	}

	public Boolean getChecked() {
		return checked;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public List<XTreeNode> getData() {
		return data;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public void setData(List<XTreeNode> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return toJson();
	}
	
	public String toJson() {
		return JsonKit.toJson(this);
	}
}
