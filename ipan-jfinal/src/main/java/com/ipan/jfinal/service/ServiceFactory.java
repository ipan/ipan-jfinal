package com.ipan.jfinal.service;

import com.jfinal.aop.Aop;
import com.jfinal.log.Log;

/**
 * Service工厂
 * 
 * JFinal4.2去掉了cglib，使用自行实现的一套基于拦截器的代理类生成方式；
 * 针对动态字节码生成、json处理，这些都有比较成熟的产品，所以，没有使用JFinal自带的功能，这样心理更踏实！
 * 我相信专业的人干专业的事，不可能一个东西什么都完美！
 * 
 * @author iPan
 * @version 2018-02-13
 */
public class ServiceFactory {
	
	private static final Log LOG = Log.getLog(ServiceFactory.class);
	
	public static <T> T create(Class<T> targetClass) {
		return Aop.get(targetClass);
	}
	
	public static <T> T createService(Class<T> targetClass) {
		T obj = null;
		try {
			obj = (T)targetClass.newInstance();
		} catch (Exception e) {
			LOG.error("创建Service失败" + targetClass, e);
		}
		return obj;
	}
	
//	public static <T> T createEnhancerService(Class<T> targetClass) {
//		return (T)Enhancer.enhance(targetClass);
//	}
	
}
