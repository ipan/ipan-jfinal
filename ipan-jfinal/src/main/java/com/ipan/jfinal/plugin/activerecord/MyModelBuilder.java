package com.ipan.jfinal.plugin.activerecord;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.ipan.jfinal.annotation.Crypto;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.ModelBuilder;

/**
 * 
 * ModelBuilder带解密
 * 
 * 参考com.jfinal.plugin.activerecord.ModelBuilder
 * 
 * @author iPan
 * @date 2021-12-07
 */
public class MyModelBuilder extends ModelBuilder {

	private MyCryptoHandler handler = null;
	
	public MyModelBuilder() {}

	public MyModelBuilder(MyCryptoHandler handler) {
		this.handler = handler;
	}
	
	public MyCryptoHandler getHandler() {
		return handler;
	}

	public void setHandler(MyCryptoHandler handler) {
		this.handler = handler;
	}
	
	private String decrypt(Crypto crypto, String colName, String value) {
		if (crypto == null) return value;
		String[] values = crypto.value();
		if (values == null || values.length < 1) {
			return value;
		}
		
		// 检测是否加密
		for (String attName : values) {
			if (colName.equals(attName)) {
				return handler.decrypt(value); // 加密
			}
		}
		// 不用加密
		return value;
	}
	
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public <T> List<T> build(ResultSet rs, Class<? extends Model> modelClass, Function<T, Boolean> func) throws SQLException, ReflectiveOperationException {
		List<T> result = new ArrayList<T>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		String[] labelNames = new String[columnCount + 1];
		int[] types = new int[columnCount + 1];
		buildLabelNamesAndTypes(rsmd, labelNames, types);
		Crypto crypto = modelClass.getAnnotation(Crypto.class);
		while (rs.next()) {
			Model<?> ar = modelClass.newInstance();
			Map<String, Object> attrs = ar.toMap(); // ar._getAttrs();
			for (int i=1; i<=columnCount; i++) {
				Object value;
				if (types[i] < Types.BLOB) {
					value = rs.getObject(i);
				} else {
					if (types[i] == Types.CLOB) {
						value = handleClob(rs.getClob(i));
					} else if (types[i] == Types.NCLOB) {
						value = handleClob(rs.getNClob(i));
					} else if (types[i] == Types.BLOB) {
						value = handleBlob(rs.getBlob(i));
					} else {
						value = rs.getObject(i);
					}
				}
				// 解密处理
				attrs.put(labelNames[i], (crypto == null || !(value instanceof String)) ? value : decrypt(crypto, labelNames[i], (String)value));
			}
			
			if (func == null) {
				result.add((T)ar);
			} else {
				if ( ! func.apply((T)ar) ) {
					break ;
				}
			}
		}
		return result;
	}
	
}
