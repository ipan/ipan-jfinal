package com.ipan.jfinal.plugin.userinfo;

import javax.servlet.http.HttpServletRequest;

/**
 * 登录用户信息管理器
 * 
 * 用户会话、角色、权限拦截统一封装，调用者自己实现接口；
 * 
 * 为了满足前后端分离，前端系统的入口与后端系统的入口不同，业务不同，拦截处理不同，所以，前端系统与后端系统的UUID分成两个，会话独立管理；
 * 所以，增加了参数mark用于区分！
 * 比如：后端 manager、前端 user来区分；
 * 								——2021-11-25
 * 
 * @author iPan
 * @version 2018-04-06
 */
public class UserInfoManager {
	
	private static UserInfoHandler handler = null;
//	private static IConvertUserInfo converter = null;
	private static RoleValidator roleValidator = null;
	private static PermissionValidator permissionValidator = null;
	
	public static void init(UserInfoHandler h, RoleValidator v, PermissionValidator pv) {
		handler = h;
//		converter = c;
		roleValidator = v;
		permissionValidator = pv;
	}
	
	public static UserInfoHandler getHandler() {
		if (handler == null) {
			throw new RuntimeException("handler is uninitialized");
		}
		return handler;
	}

//	public static IConvertUserInfo getConverter() {
//		if (converter == null) {
//			throw new RuntimeException("converter is uninitialized");
//		}
//		return converter;
//	}

	public static RoleValidator getRoleValidator() {
		if (roleValidator == null) {
			throw new RuntimeException("roleValidator is uninitialized");
		}
		return roleValidator;
	}
	
	public static PermissionValidator getPermissionValidator() {
		if (permissionValidator == null) {
			throw new RuntimeException("permissionValidator is uninitialized");
		}
		return permissionValidator;
	}

	public static UserInfo getUserInfo(HttpServletRequest httpRequest) { // 是否有权限，单一会话场景；使用VUE彻底前后端分离，后端管理系统与前端系统合并的场景；
		return getHandler().getUserInfo(httpRequest, null);
	}
	public static UserInfo getUserInfo(HttpServletRequest httpRequest, String mark) { // 后端管理系统与前端VUE系统分离，后端管理系统使用传统的cookie，VUE使用http head，所以需要使用mark区分；
		return getHandler().getUserInfo(httpRequest, mark);
	}

	public static void putUserInfo(HttpServletRequest httpRequest, UserInfo userInfo) { // 是否有权限，单一会话场景；
		getHandler().putUserInfo(httpRequest, userInfo, null);
	}
	public static void putUserInfo(HttpServletRequest httpRequest, UserInfo userInfo, String mark) {
		getHandler().putUserInfo(httpRequest, userInfo, mark);
	}

	public static void removeUserInfo(HttpServletRequest httpRequest) { // 是否有权限，单一会话场景；
		getHandler().removeUserInfo(httpRequest, null);
	}
	public static void removeUserInfo(HttpServletRequest httpRequest, String mark) {
		getHandler().removeUserInfo(httpRequest, mark);
	}
	
	public static void removeUserInfo(String uuid) {
		getHandler().removeUserInfo(uuid, null);
	}
	public static void removeUserInfo(String uuid, String mark) {
		getHandler().removeUserInfo(uuid, mark);
	}
	
	public static boolean hasRole(HttpServletRequest httpRequest, String... roleCodes) { // 是否有权限，单一会话场景；
		return hasRole(httpRequest, null, roleCodes);
	}
	public static boolean hasRole(HttpServletRequest httpRequest, String mark, String... roleCodes) {
		UserInfo user = getUserInfo(httpRequest, mark);
		if (user == null) {
			return false;
		}
		return getRoleValidator().hasRole(user.getRoles(), roleCodes);
	}
	
	public static boolean hasAdminSuperRole(HttpServletRequest httpRequest) { // 是否有权限，单一会话场景；
		return hasAdminSuperRole(httpRequest, null);
	}
	public static boolean hasAdminSuperRole(HttpServletRequest httpRequest, String mark) {
		UserInfo user = getUserInfo(httpRequest, mark);
		if (user == null) {
			return false;
		}
		return getRoleValidator().hasAdminSuperRole(user.getRoles());
	}
	
	public static boolean hasPermissionOnly(HttpServletRequest httpRequest, String... codes) { // 是否有权限，单一会话场景；
		return hasPermission(httpRequest, null, codes);
	}
	public static boolean hasPermission(HttpServletRequest httpRequest, String mark, String... codes) {
		UserInfo user = getUserInfo(httpRequest, mark);
		if (user == null) {
			return false;
		}
		return getPermissionValidator().hasPermission(httpRequest, mark, codes);
	}

}
