package com.ipan.jfinal.simpleSso;

import java.text.ParseException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ipan.jfinal.controller.ResultDefineManager;
import com.ipan.kits.mapper.FastJsonMapper;
import com.ipan.kits.web.ServletUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;

/**
 * 单点登录拦截器
 * 
 * 校验客户端get/post提交的参数是否合法以及是否被篡改
 * 
 * @author iPan
 * @date 2022-07-26
 */
public abstract class SimpleSSOInterceptor implements Interceptor {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	private FastJsonMapper jsonNullMapper = FastJsonMapper.me().createWriteNullMapper(); // 必须支持输出null值

	@Override
	public void intercept(Invocation inv) {
		Controller con = inv.getController();
		HttpServletRequest request = con.getRequest();
		String appid = request.getParameter(SimpleSSOPacker.APPID_NAME);
		String userid = request.getParameter(SimpleSSOPacker.USERID_NAME);
		String timestamp = request.getParameter(SimpleSSOPacker.TIMESTAMP_NAME);
		String appkey = getAppkey(userid); // 私钥
		Map map = request.getParameterMap();
		
		// 时间校验
		boolean success = false;
		try {
			success = SimpleSSOPacker.validateTimestamp(timestamp, getDiffSecond());
		} catch (ParseException e) {
			String ip = ServletUtil.getClientIp(request);
			String userAgent = request.getHeader("User-Agent");
			log.info("单点登录时间格式校验失败，参数={}，IP={}，UserAgent={}", jsonNullMapper.toJson(map), ip, userAgent);
			retErrorAction(request, con, "1001", "时间格式校验失败");
			return ;
		}
		if (!success) {
			String ip = ServletUtil.getClientIp(request);
			String userAgent = request.getHeader("User-Agent");
			log.info("单点登录时间超时，参数={}，IP={}，UserAgent={}", jsonNullMapper.toJson(map), ip, userAgent);
			retErrorAction(request, con, "1002", "时间超时");
			return ;
		}
		
		// 签名校验
		success = SimpleSSOPacker.validate(map, isOffset(), appid, userid, appkey, getOffset());
		if (success) {
			inv.invoke();
		} else {
			String ip = ServletUtil.getClientIp(request);
			String userAgent = request.getHeader("User-Agent");
			log.info("单点登录签名校验失败，参数={}，IP={}，UserAgent={}", jsonNullMapper.toJson(map), ip, userAgent);
			retErrorAction(request, con, "1003", "签名校验失败");
		}
	}
	
	private void retErrorAction(HttpServletRequest request, Controller con, String code, String msg) {
		boolean isAjax = ServletUtil.isAjax(request);
		if (isAjax) {
			Kv param = Kv.by(ResultDefineManager.me().getCodeKey(), code)
					.set(ResultDefineManager.me().getMsgKey(), msg);
			con.renderJson(param);
		} else {
			con.setAttr(ResultDefineManager.me().getCodeKey(), code);
			con.setAttr(ResultDefineManager.me().getMsgKey(), msg);
			Integer errorCode = getErrorCode();
			if (errorCode == null) {
				con.renderError(400);
			} else {
				con.set("errorMsg", msg); // errorMsg写死
				con.renderError(errorCode);
			}
		}
	}
	
	/**
	 * 获取appkey
	 * @param userid 根据userid获取
	 * @return appkey
	 */
	public abstract String getAppkey(String userid);
	
	/**
	 * 是否需要偏移量
	 * @return true 是 false 否
	 */
	public abstract boolean isOffset();
	
	/**
	 * 获取偏移量
	 * @return 返回偏移量
	 */
	public abstract long getOffset();
	
	/**
	 * 获取时间校验的间隔秒数
	 * @return 间隔秒
	 */
	public abstract int getDiffSecond();
	
	/**
	 * 获取错误页面链接
	 * @return 错误页面链接
	 */
	public abstract Integer getErrorCode();

}
