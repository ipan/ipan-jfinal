package com.ipan.jfinal.plugin.userinfo;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户信息处理器接口
 * 
 * 为了满足前后端分离，前端系统的入口与后端系统的入口不同，业务不同，拦截处理不同，所以，前端系统与后端系统的UUID分成两个，会话独立管理；
 * 所以，增加了参数mark用于区分！
 * 前端会话，使用http head；后端会话，使用cookie；
 * 比如：后端 manager、前端 user来区分；
 * 								——2021-11-25
 * 
 * @author iPan
 * @version 2018-04-06
 */
public interface UserInfoHandler {
	
	public void putUserInfo(HttpServletRequest httpRequest, UserInfo userInfo, String mark);
	
	public void removeUserInfo(HttpServletRequest httpRequest, String mark);
	public void removeUserInfo(java.io.Serializable uuid, String mark);
	
	public UserInfo getUserInfo(HttpServletRequest httpRequest, String mark);
	public UserInfo getUserInfo(java.io.Serializable uuid, String mark);
	
}
