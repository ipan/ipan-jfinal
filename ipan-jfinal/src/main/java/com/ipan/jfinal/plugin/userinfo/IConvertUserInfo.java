package com.ipan.jfinal.plugin.userinfo;

/**
 * 用户信息转换器
 * 
 * 将系统的对象转换为UserInfo插件的对象
 * 
 * @author iPan
 * @version 2018-05-13
 * 
 * @deprecated 不需要转换，自行转换后再调接口；
 */
public interface IConvertUserInfo {

	public UserInfo convert(Object obj);
	
}
