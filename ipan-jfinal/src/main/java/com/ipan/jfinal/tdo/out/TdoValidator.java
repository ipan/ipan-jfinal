package com.ipan.jfinal.tdo.out;

/**
 * 源数据格式校验器
 * 
 * @author iPan
 * @date 2023-05-03
 */
public interface TdoValidator {
	
	/** 默认不校验 */
	public static final TdoValidator DEFAULT_VALIDATOR = new TdoValidator() {
		@Override
		public String validate(Object row, int keyType) {
			return "";
		}
	};

	/**
	 * 校验一行数据
	 * excel文件读取时候，返回Map<Integer, String>
	 * Json字符串时候，返回Map<String, Object>
	 * 
	 * @param row 一行源数据Map类型
	 * @param keyType 映射key类型（1 数字key 2 字符串key）
	 * @return 成功，返回空字符串；失败，返回错误提示；
	 */
	public String validate(Object row, int keyType);
	
}
