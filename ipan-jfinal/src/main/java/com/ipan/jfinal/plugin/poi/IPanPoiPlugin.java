package com.ipan.jfinal.plugin.poi;

import com.ipan.poi.PoiConfig;
import com.jfinal.kit.Prop;
import com.jfinal.plugin.IPlugin;

/**
 * excel报表导入导出插件
 * 
 * 配置文件方式改为Java文件配置
 * # ipan_poi配置
 * # 日志文件保存目录（默认：/ipan_poi/excel）
 * #ipan.poi.excel.log.dir=/jfdemo/ipan_poi/excel
 * # 导入excel日志是否开启（默认：true）
 * #ipan.poi.excel.log.xlsImporter.enable=true
 * # 导出excel日志是否开启（默认：true）
 * #ipan.poi.excel.log.xlsExporter.enable=true
 * # 导入是否需要验证（默认：true）
 * #ipan.poi.xlsImporter.validate=true
 * # 命名解析策略（默认：com.ipan.poi.orm.DefaultNamingStrategy）
 * #ipan.poi.namingStrategy=com.ipan.poi.orm.DefaultNamingStrategy
 * 
 * @author iPan
 * @version 2018-05-07
 */
public class IPanPoiPlugin implements IPlugin {
	
	public static final String BASE_PATH = "/jfinal";
	private Prop prop = null;
	
	public IPanPoiPlugin(Prop prop) {
		this.prop = prop;
	}

	@Override
	public boolean start() {
		PoiConfig config = PoiConfig.getInstance();
		// 日志文件保存目录（默认：/ipan_poi/excel）
		config.setExcelLogDir(prop.get("ipan.poi.excel.log.dir", BASE_PATH + "/ipan_poi/excel")); // 默认/jfinal/ipan_poi/excel
		// 导入excel日志是否开启（默认：true）
		config.setXlsImporterLog(prop.getBoolean("ipan.poi.excel.log.xlsImporter.enable", true));
		// 导出excel日志是否开启（默认：true）
		config.setXlsExporterLog(prop.getBoolean("ipan.poi.excel.log.xlsExporter.enable", true));
		// 导入是否需要验证（默认：true）
		config.setXlsImporterValidate(prop.getBoolean("ipan.poi.xlsImporter.validate", true));
		// 命名解析策略（默认：com.ipan.poi.orm.DefaultNamingStrategy）
		config.setNamingStrategy(prop.get("ipan.poi.namingStrategy", "com.ipan.poi.orm.DefaultNamingStrategy"));
		return true;
	}

	@Override
	public boolean stop() {
		// TODO: 是否删除垃圾文件？？？
		return true;
	}

}
