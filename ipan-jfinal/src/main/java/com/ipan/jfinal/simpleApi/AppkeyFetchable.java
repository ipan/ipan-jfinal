package com.ipan.jfinal.simpleApi;

import java.util.Map;

/**
 * 根据userid获取appkey
 * 
 * @author iPan
 * @date 2022-06-29
 */
public interface AppkeyFetchable {
	
	/**
	 * 获取Appid
	 * @return 返回appid
	 */
	public String getAppid();
	
	/**
	 * 获取appkey
	 * 
	 * @param appid APPID
	 * @param userid 用户ID
	 * @return appkey
	 */
	public String getAppkey(String userid);
	
	/**
	 * 设置appkey
	 * 
	 * @param userid 用户ID
	 * @param appkey appkey
	 */
	public void putAppkey(String userid, String appkey);
	
	/**
	 * 删除appkey
	 * @param userid 用户ID
	 */
	public void removeAppkey(String userid);
	
	/**
	 * 初始化appkey
	 * @param initMap 初始化Map
	 */
	public void initAppkey(Map<String, String> initMap);
	
	/**
	 * 清空缓存
	 */
	public void clear();
	
	/**
	 * 缓存数量
	 * @return 缓存总数
	 */
	public int size();
	
}
