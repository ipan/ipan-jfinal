package com.ipan.jfinal.render;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.ipan.jfinal.controller.ResultDefineManager;
import com.ipan.kits.web.ServletUtil;
import com.jfinal.kit.Okv;
import com.jfinal.render.Render;
import com.jfinal.render.RenderException;
import com.jfinal.render.RenderManager;

/**
 * 自定义异常渲染 根据前端请求的是HTML、Json来做不同的渲染；
 * 
 * @author iPan
 * @version 2022-05-05
 */
public class MyErrorRender extends Render {

	protected static final String contentTypeHtml = "text/html; charset=" + getEncoding();
	protected static final String contentTypeJson = "application/json; charset=" + getEncoding();
	
	protected static final String version = ""; // 版本去掉了
	
	protected static final byte[] html404 = ("<html><head><title>404 Not Found</title></head><body bgcolor='white'><center><h1>404 Not Found</h1></center><hr>" + version + "</body></html>").getBytes();
	protected static final byte[] html500 = ("<html><head><title>500 Internal Server Error</title></head><body bgcolor='white'><center><h1>500 Internal Server Error</h1></center><hr>" + version + "</body></html>").getBytes();
	protected static final byte[] html400 = ("<html><head><title>400 Bad Request</title></head><body bgcolor='white'><center><h1>400 Bad Request</h1></center><hr>" + version + "</body></html>").getBytes();
	protected static final byte[] html401 = ("<html><head><title>401 Unauthorized</title></head><body bgcolor='white'><center><h1>401 Unauthorized</h1></center><hr>" + version + "</body></html>").getBytes();
	protected static final byte[] html403 = ("<html><head><title>403 Forbidden</title></head><body bgcolor='white'><center><h1>403 Forbidden</h1></center><hr>" + version + "</body></html>").getBytes();
	// 注意：当前系统的Json格式state、msg可能不一样，需要通过ResultDefineManager去自定义！
	protected static byte[] json404 = null; // Okv.of("state", "fail").set("msg", "404 Not Found").toJson().getBytes();
	protected static byte[] json500 = null; // Okv.of("state", "fail").set("msg", "500 Internal Server Error").toJson().getBytes();
	protected static byte[] json400 = null; // Okv.of("state", "fail").set("msg", "400 Bad Request").toJson().getBytes();
	protected static byte[] json401 = null; // Okv.of("state", "fail").set("msg", "401 Unauthorized").toJson().getBytes();
	protected static byte[] json403 = null; // Okv.of("state", "fail").set("msg", "403 Forbidden").toJson().getBytes();
	
	// 前后端分离的项目，不需要兼容HTML的返回，增加一个参数配置
	protected static boolean onlyJson = false;
	
	protected static final Map<Integer, byte[]> errorHtmlMap = new HashMap<>();
	protected static final Map<Integer, byte[]> errorJsonMap = new HashMap<>();
	protected static final Map<Integer, String> errorViewMap = new HashMap<Integer, String>();
	
	static {
		errorHtmlMap.put(404, html404);
		errorHtmlMap.put(500, html500);
		errorHtmlMap.put(400, html400);
		errorHtmlMap.put(401, html401);
		errorHtmlMap.put(403, html403);
		
		// 转换为系统的code，提示内容可以在外部再次覆盖；
		json404 = Okv.of(ResultDefineManager.me().getCodeKey(), ResultDefineManager.me().getFail().getLeft()).set(ResultDefineManager.me().getMsgKey(), "404 Not Found").toJson().getBytes();
		json500 = Okv.of(ResultDefineManager.me().getCodeKey(), ResultDefineManager.me().getFail().getLeft()).set(ResultDefineManager.me().getMsgKey(), "500 Internal Server Error").toJson().getBytes();
		json400 = Okv.of(ResultDefineManager.me().getCodeKey(), ResultDefineManager.me().getFail().getLeft()).set(ResultDefineManager.me().getMsgKey(), "400 Bad Request").toJson().getBytes();
		json401 = Okv.of(ResultDefineManager.me().getCodeKey(), ResultDefineManager.me().getFail().getLeft()).set(ResultDefineManager.me().getMsgKey(), "401 Unauthorized").toJson().getBytes();
		json403 = Okv.of(ResultDefineManager.me().getCodeKey(), ResultDefineManager.me().getFail().getLeft()).set(ResultDefineManager.me().getMsgKey(), "403 Forbidden").toJson().getBytes();
		errorJsonMap.put(404, json404);
		errorJsonMap.put(500, json500);
		errorJsonMap.put(400, json400);
		errorJsonMap.put(401, json401);
		errorJsonMap.put(403, json403);
	}
	
	protected int errorCode;
	protected String viewOrJson;
	
	public MyErrorRender(int errorCode, String viewOrJson) {
		this.errorCode = errorCode;
		this.viewOrJson = viewOrJson;
	}
	
	public MyErrorRender(int errorCode) {
		this(errorCode, null);
	}
	
	public static boolean isOnlyJson() {
		return onlyJson;
	}

	public static void setOnlyJson(boolean onlyJson) {
		MyErrorRender.onlyJson = onlyJson;
	}

	/**
	 * 设置异常发生时响应的错误页面
	 */
	public static void setErrorView(int errorCode, String errorView) {
		errorViewMap.put(errorCode, errorView);
	}
	
	public static String getErrorView(int errorCode) {
		return errorViewMap.get(errorCode);
	}
	
	/**
	 * 设置异常发生时响应的 html 内容
	 */
	public static void setErrorHtmlContent(int errorCode, String htmlContent) {
		try {
			errorHtmlMap.put(errorCode, htmlContent.getBytes(getEncoding()));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 设置异常发生时响应的 json 内容
	 */
	public static void setErrorJsonContent(int errorCode, String jsonContent) {
		try {
			errorJsonMap.put(errorCode, jsonContent.getBytes(getEncoding()));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void render() {
//		String ct = request.getContentType();
//		boolean isJsonContentType = ct != null && ct.indexOf("json") != -1;
		boolean isJsonContentType = (isOnlyJson()) ? true : ServletUtil.isAjax(request); // 以上判断不正确，ajax提交有可能是application/x-www-form-urlencoded;类型
		if (isJsonContentType) { // 不是json才设置，否则json无法正常接收！
			response.setStatus(200); // ajax请求响应必须是200才能正常接收
		} else {
			response.setStatus(getErrorCode());	// HttpServletResponse.SC_XXX_XXX
		}
		
		// 支持 me.setErrorView(xxx.html) 配置
		// 注意：针对 json 的 setErrorJsonContent(...) 直接覆盖掉了默认值，会走后面的 response.getOutputStream().write(...)
		if (viewOrJson == null) {
		    if (!isJsonContentType) {
		        viewOrJson = getErrorView(getErrorCode());
		    }
		}
		
		// render with viewOrJson
		if (viewOrJson != null) {
			if (isJsonContentType) {
				RenderManager.me().getRenderFactory().getJsonRender(viewOrJson).setContext(request, response).render();
			} else {
				RenderManager.me().getRenderFactory().getRender(viewOrJson).setContext(request, response).render();
			}
			return;
		}
		
		// render with html content
		try {
			response.setContentType(isJsonContentType ? contentTypeJson : contentTypeHtml);
			response.getOutputStream().write(isJsonContentType ? getErrorJson() : getErrorHtml());
		} catch (IOException e) {
			throw new RenderException(e);
		}
	}
	
	public byte[] getErrorHtml() {
		byte[] ret = errorHtmlMap.get(getErrorCode());
		return ret != null ? ret : ("<html><head><title>" + errorCode + " Error</title></head><body bgcolor='white'><center><h1>" + errorCode + " Error</h1></center><hr>" + version + "</body></html>").getBytes();
	}
	
	public byte[] getErrorJson() {
		byte[] ret = errorJsonMap.get(getErrorCode());
		return ret != null ? ret : Okv.of(ResultDefineManager.me().getCodeKey(), ResultDefineManager.me().getFail().getLeft()).set(ResultDefineManager.me().getMsgKey(), errorCode + " Error").toJson().getBytes();
	}
	
	public int getErrorCode() {
		return errorCode;
	}
}
