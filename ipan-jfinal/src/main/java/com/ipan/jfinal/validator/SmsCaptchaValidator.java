package com.ipan.jfinal.validator;

import com.ipan.jfinal.controller.ResultDefineManager;
import com.jfinal.captcha.Captcha;
import com.jfinal.captcha.CaptchaManager;
import com.jfinal.captcha.ICaptchaCache;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * 短信验证码+图形验证码_通用校验器
 * 
 * 一般用不到，一般是用图形验证码+手机号获取短信验证码，用短信验证码+手机号直接登录；
 * 
 * @author iPan
 * @date 2023-02-01
 * @deprecated 用不到
 */
public class SmsCaptchaValidator extends Validator {
	
	protected String k_sms_key = "smsKey";
	protected String k_sms_value = "smsValue";
	protected String k_validate_code = "validateCode";
	protected String k_error_key = "msg";
	protected String k_code_key = "code";

	public SmsCaptchaValidator() {}
	
	@Override
	protected void validate(Controller c) {
		setShortCircuit(true); // 短路校验
		
		// 表单提交参数
		String smsKey = c.getPara(k_sms_key); // 短信验证码Key
		String smsValue = c.getPara(k_sms_value); // 短信验证码Value
		validateCaptcha(k_validate_code, k_error_key, "图形验证码不正确"); // 验证码验证成功后会删除服务端缓存中的验证码，并要求客户端删除cookie；
		
		// 校验手机验证码
		ICaptchaCache captchaCache = CaptchaManager.me().getCaptchaCache();
		Captcha captcha = captchaCache.get(smsKey);
		if (captcha != null && captcha.notExpired() && captcha.getValue().equalsIgnoreCase(smsValue)) {
			captchaCache.remove(captcha.getKey());
		} else {
			addError(k_error_key, "手机验证码不正确"); // 标识校验失败
		}
	}

	@Override
	protected void handleError(Controller c) {
		c.setAttr(k_code_key, ResultDefineManager.me().getFail().getLeft()); // json状态码
		c.renderJson();
	}

}
