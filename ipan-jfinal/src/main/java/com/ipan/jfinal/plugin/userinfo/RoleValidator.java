package com.ipan.jfinal.plugin.userinfo;

import java.util.List;

/**
 * 角色验证器
 * 
 * @author iPan
 * @version 2018-05-13
 */
public interface RoleValidator {

	public boolean hasRole(List<String> curRoles, String... targetRole);
	
	public boolean hasAdminSuperRole(List<String> roles);
}
