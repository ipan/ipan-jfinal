package com.ipan.jfinal.tdo.config;

import java.util.ArrayList;
import java.util.List;

/**
 * 模板配置
 * 
 * @author iPan
 * @date 2023-05-02
 */
public class Tpl {
	/** 1 映射key是数字 */
	public static final int KEY_TYPE_1 = 1;
	/** 2 映射key是字符串 */
	public static final int KEY_TYPE_2 = 2;
	
	int index = 0;
	int keyType = 1; // 1 映射key是数字 2 映射key是字符串
	String type = null;
	List<Mp> mappings = null;
	
	public Tpl() {}
	
	public Tpl(int index, int keyType, String type) {
		this.index = index;
		this.keyType = keyType;
		this.type = type;
	}
	
	public Tpl(int keyType, String type) {
		this.keyType = keyType;
		this.type = type;
	}

	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getKeyType() {
		return keyType;
	}
	public void setKeyType(int keyType) {
		this.keyType = keyType;
	}
	public List<Mp> getMappings() {
		return mappings;
	}
	public void setMappings(List<Mp> mappings) {
		this.mappings = mappings;
	}
	
	public synchronized Tpl addMapping(String toVal, String toLabel, String srcVal, String srcLabel, String format) {
		if (mappings == null) {
			mappings = new ArrayList<Mp>();
		}
		mappings.add(new Mp(toVal, toLabel, srcVal, srcLabel, format));
		return this;
	}

	public static Tpl create() {
		return new Tpl();
	}
	
	public static Tpl create(int keyType, String type) {
		return new Tpl(keyType, type);
	}
	
	public static Tpl create(int index, int keyType, String type) {
		return new Tpl(index, keyType, type);
	}
	
}
