package com.ipan.jfinal.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 角色验证
 * 
 * @author iPan
 * @version 2014-4-26
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiresRoles {

	/** 角色 */
	String[] value() default "";
	
	/** 选项（true: 存在该角色，默认； false: 不存在该角色；） */
	boolean existed() default true;
}
