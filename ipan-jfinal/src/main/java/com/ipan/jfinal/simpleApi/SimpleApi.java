package com.ipan.jfinal.simpleApi;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 接口调用次数
 * 
 * @author iPan
 * @date 2023-05-31
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleApi {

	/**
     * 最大调用次数
     */
    public int maxCount() default 10000;
    
    /**
     * 接口名称
     */
    public String name();
    
    /**
     * 接口访问权限
     */
    public String role() default "";
    
}
