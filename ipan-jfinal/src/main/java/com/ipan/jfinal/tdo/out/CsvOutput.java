package com.ipan.jfinal.tdo.out;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVPrinter;

import com.alibaba.excel.util.StringUtils;
import com.ipan.jfinal.tdo.OutputKits;
import com.ipan.jfinal.tdo.config.Mp;
import com.ipan.jfinal.tdo.config.Tpl;
import com.ipan.jfinal.tdo.transform.Transducer;
import com.ipan.kits.base.ExceptionUtil;
import com.ipan.kits.io.FileUtil;
import com.ipan.kits.mapper.ExcelCsvMapper;
import com.ipan.kits.text.Charsets;

/**
 * 输出到CSV文件
 * 
 * @author iPan
 * @date 2023-05-02
 */
public class CsvOutput extends BaseOutput {
	protected File fileOut = null;
	protected CSVPrinter printer = null;
	
	public CsvOutput(Tpl tpl, List data, File fout) {
		super(tpl, data);
		try {
			fileOut = new File(fout.getAbsolutePath() + ".tmp"); // 目标文件名后增加.tmp
			printer = ExcelCsvMapper.me().print(fileOut, Charsets.UTF_8);
		} catch (IOException e) {
			ExceptionUtil.unchecked(e);
		}
	}
	
	@Override
	public void execute() {
		if (tpl == null || data == null || data.size() < 1) return ;
		// 写入标题
		printHead();
		List<Mp> mapList = tpl.getMappings();
		// 写入内容
		try {
			for (int i=0, len=data.size(); i<len; i++) {
				Map row = (Map) data.get(i);
				String errMsg = validator.validate(row, tpl.getKeyType());
				if (StringUtils.isNotBlank(errMsg)) { // 格式校验
					throw new java.lang.IllegalArgumentException(errMsg);
				}
				// 格式转换
				Map retData = Transducer.me().transform(row, i+1, tpl); // 同步方式、异步方式的转换器是一致的
				// 写入CSV文件
				for (Mp mp : mapList) {
					String targetVal = OutputKits.getCellValue(retData, mp, tpl.getKeyType()); // 同步方式、异步方式的转换器是一致的
					printer.print(targetVal);
				}
				printer.println();
			}
		} catch (Exception e) {
			ExceptionUtil.unchecked(e);
		} finally {
			// 文件改名
			if (fileOut != null && fileOut.exists() && fileOut.getName().endsWith(".tmp")) {
				String path = fileOut.getAbsolutePath();
				String reTxtFileName = path.substring(0, path.lastIndexOf(".")); // 去掉最后的.tmp
				FileUtil.overrideRenameTo(fileOut, new File(reTxtFileName));
			}
			// 关闭文件流
			if (printer != null) {
				try {
					printer.close();
				} catch (IOException e) {
				}
			}
		}
	}
	
	protected void printHead() {
		List<Mp> mapList = tpl.getMappings();
		try {
			if (mapList != null && mapList.size() > 0) {
				for (Mp item : mapList) {
					String label = item.getSrcLabel();
					printer.print(label);
				}
				printer.println();
			} 
		} catch (Exception e) {
			ExceptionUtil.unchecked(e);
		}
	}
	
}
