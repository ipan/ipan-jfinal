package com.ipan.jfinal.pojo;

import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.Model;

/**
 * 缺省Model
 * 项目中有些比较杂的功能可能会放在一个共通模块，这样就不属于具体的模块；在这种情况下，可以将Service的泛型设置为DefaultModel！
 * 
 * @author iPan
 * @version 2018-02-13
 */
public class EmptyModel extends Model<EmptyModel> implements IBean {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4615752513983910377L;

}
