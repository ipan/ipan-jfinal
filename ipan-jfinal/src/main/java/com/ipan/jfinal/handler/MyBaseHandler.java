package com.ipan.jfinal.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

/**
 * 自定义基础预处理Handler
 * 
 * 静态资源直接返回，交给web服务器自己处理；
 * 
 * @author iPan
 * @version 2018-05-04
 */
public class MyBaseHandler extends Handler {
	
	private String contextPathName = "ctx";
	
	public MyBaseHandler() {}
	
	public MyBaseHandler(String contextPathName) {
		this.contextPathName = contextPathName;
	}

	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
//		int index = target.lastIndexOf(".jsp"); // 若要定义.jsp的请求映射到Controller，可以这样做；
//		if (index != -1) {
//			target = target.substring(0, index);
//		}
		request.setAttribute(contextPathName, request.getContextPath());
		// 静态资源直接返回
		if (target.indexOf('.') != -1) {
			return ;
		}
		next.handle(target, request, response, isHandled);
	}
}
