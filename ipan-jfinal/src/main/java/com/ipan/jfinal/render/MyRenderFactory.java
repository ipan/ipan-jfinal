package com.ipan.jfinal.render;

import java.io.File;

import com.jfinal.kit.Okv;
import com.jfinal.render.Render;
import com.jfinal.render.RenderFactory;

/**
 * 自定义渲染工厂
 * Constants中setErrorView/getErrorView可以自定义全局异常Html页面，但是不能同时支持Html与Json的异常响应；
 * 所以，Constants的这一功能暂时不使用，全局Html页面直接定义在MyErrorRender中并且支持Json格式的响应；
 * 
 * @author iPan
 * @version 2018-05-06
 */
public class MyRenderFactory extends RenderFactory {
	
	/**
	 * 定义Json返回数据的code、message对应的KEY，以及错误码（为了方便调整code、msg的键值）；
	 * 若要定义不同错误码对应的具体的提示，请使用MyErrorRender的setErrorJsonContent方法；
	 * 
	 * 比如：
	 * 	MyRenderFactory.initJsonErrorCode(
	 * 			ResultDefineManager.me().getCodeKey(),
	 * 			ResultDefineManager.me().getFail().getLeft(), 
	 * 			ResultDefineManager.me().getMsgKey());
	 * 	RenderManager.me().setRenderFactory(new MyRenderFactory()); // 自定义渲染工厂
	 * 
	 * @param errCodeKey 错误码Key
	 * @param errCodeValue 错误码Value
	 * @param errMsgKey 错误码消息Key
	 */
	public static void initJsonErrorCode(String errCodeKey, String errCodeValue, String errMsgKey) {
		String json404 = Okv.of(errCodeKey, errCodeValue).set(errMsgKey, "404 Not Found").toJson();
		String json500 = Okv.of(errCodeKey, errCodeValue).set(errMsgKey, "500 Internal Server Error").toJson();
		String json400 = Okv.of(errCodeKey, errCodeValue).set(errMsgKey, "400 Bad Request").toJson();
		String json401 = Okv.of(errCodeKey, errCodeValue).set(errMsgKey, "401 Unauthorized").toJson();
		String json403 = Okv.of(errCodeKey, errCodeValue).set(errMsgKey, "403 Forbidden").toJson();
		MyErrorRender.setErrorJsonContent(404, json404);
		MyErrorRender.setErrorJsonContent(500, json500);
		MyErrorRender.setErrorJsonContent(400, json400);
		MyErrorRender.setErrorJsonContent(401, json401);
		MyErrorRender.setErrorJsonContent(403, json403);
	}

	@Override
	public Render getErrorRender(int errorCode, String view) { // JFinal 5.0以上支持了这块功能
		return new MyErrorRender(errorCode, view);
	}
	
	@Override
	public Render getErrorRender(int errorCode) { // JFinal 5.0以上支持了这块功能
		return new MyErrorRender(errorCode);
	}
	
	public Render getImageRender(File file) {
		return new ImageRender(file);
	}
	
	public Render getHeadCaptchaRender() {
		return new MyCaptchaRender();
	}
	
	public Render getTextFileRender(String text, String downloadFileName) {
		return new MyTextFileRender(text, downloadFileName);
	}
	public Render getTextFileRender(String text, String downloadFileName, String contentType, String encode) {
		return new MyTextFileRender(text, downloadFileName, contentType, encode);
	}
	
}
