package com.ipan.jfinal.plugin.druid;

import com.jfinal.plugin.druid.DruidPlugin;

/**
 * 支持Druid1.2.12版本以上增加的connectTimeout和socketTimeout配置
 * 
 * @author iPan
 * @date 2023-12-13
 */
public class DruidPlugin_1_2_12 extends DruidPlugin {
	
//	protected Integer connectTimeout = DruidAbstractDataSource.DEFAULT_TIME_CONNECT_TIMEOUT_MILLIS;
//	protected Integer socketTimeout = DruidAbstractDataSource.DEFAULT_TIME_SOCKET_TIMEOUT_MILLIS;
	protected Integer connectTimeout = null; // 毫秒
	protected Integer socketTimeout = null; // 毫秒
	protected Integer queryTimeout = null; // 秒
    protected Integer transactionQueryTimeout = null; // 秒
    protected Long maxEvictableIdleTimeMillis = 1000L * 60L * 60L * 7; // 【最大空闲时间】连接空闲时间大于该值，不管minIdle都关闭该连接
	
	public DruidPlugin_1_2_12(String url, String username, String password) {
		super(url, username, password);
	}

	public DruidPlugin_1_2_12(String url, String username, String password, String driverClass, String filters) {
		super(url, username, password, driverClass, filters);
	}

	public DruidPlugin_1_2_12(String url, String username, String password, String driverClass) {
		super(url, username, password, driverClass);
	}

	public Integer getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(Integer connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public Integer getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(Integer socketTimeout) {
		this.socketTimeout = socketTimeout;
	}
	
	public Integer getQueryTimeout() {
		return queryTimeout;
	}

	public void setQueryTimeout(Integer queryTimeout) {
		this.queryTimeout = queryTimeout;
	}

	public Integer getTransactionQueryTimeout() {
		return transactionQueryTimeout;
	}

	public void setTransactionQueryTimeout(Integer transactionQueryTimeout) {
		this.transactionQueryTimeout = transactionQueryTimeout;
	}
	
	public Long getMaxEvictableIdleTimeMillis() {
		return maxEvictableIdleTimeMillis;
	}

	public void setMaxEvictableIdleTimeMillis(Long maxEvictableIdleTimeMillis) {
		this.maxEvictableIdleTimeMillis = maxEvictableIdleTimeMillis;
	}

	@Override
	public boolean start() {
		boolean ret = super.start();
		if (ret == false) return false;
		// druid1.2.12新增参数
		if (connectTimeout != null) {
			ds.setConnectTimeout(connectTimeout);
		}
		if (socketTimeout != null) {
			ds.setSocketTimeout(socketTimeout);
		}
		if (queryTimeout != null) {
			ds.setQueryTimeout(queryTimeout);
		}
		if (transactionQueryTimeout != null) {
			ds.setTransactionQueryTimeout(transactionQueryTimeout);
		}
		if (maxEvictableIdleTimeMillis != null) {
			ds.setMaxEvictableIdleTimeMillis(maxEvictableIdleTimeMillis);
		}
		return ret;
	}
	
}
