package com.ipan.jfinal.excel;

import java.lang.reflect.Method;
import java.util.Date;

import com.ipan.kits.reflect.ReflectionUtil;
import com.ipan.poi.excel.config.XlsEntity;
import com.ipan.poi.excel.hander.BaseXlsHander;
import com.ipan.poi.excel.service.XlsService;
import com.jfinal.plugin.activerecord.Model;

/**
 * Excel导入默认处理器
 * 
 * @author iPan
 * @version 2021-11-12
 */
public class DefaultXlsHander extends BaseXlsHander {

	public DefaultXlsHander(XlsService<Object> service) {
		super(service);
	}

	@Override
	public void insert(XlsEntity defEntity, Object formBean) {
		Model bean = (Model) formBean;
		insertInit(bean);
		super.insert(defEntity, formBean);
	}

	@Override
	public void update(XlsEntity defEntity, Object formBean, Object entityBean) {
		Model bean = (Model) entityBean;
		updateInit(bean);
		super.update(defEntity, formBean, entityBean);
	}
	
	protected void insertInit(Model model) {
		Date sysDate = new Date();
		Method m = null;
		try {
			m = model.getClass().getMethod("getCreateTime");
			if (m != null) {
				Object val = ReflectionUtil.invokeGetter(model, "createTime");
				if (val == null) {
					ReflectionUtil.invokeSetter(model, "createTime", sysDate);
				}
			}
		} catch (Exception e) {
		}
		try {
			m = model.getClass().getMethod("getUpdateTime");
			if (m != null) {
				Object val = ReflectionUtil.invokeGetter(model, "updateTime");
				if (val == null) {
					ReflectionUtil.invokeSetter(model, "updateTime", sysDate);
				}
			}
		} catch (Exception e) {
		}
	}
	
	protected void updateInit(Model model) {
		Date sysDate = new Date();
		Method m = null;
		try {
			m = model.getClass().getMethod("getUpdateTime");
			if (m != null) {
				Object val = ReflectionUtil.invokeGetter(model, "updateTime");
				if (val == null) {
					ReflectionUtil.invokeSetter(model, "updateTime", sysDate);
				}
			}
		} catch (Exception e) {
		}
	}
	
}
