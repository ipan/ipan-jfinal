package com.ipan.jfinal.tdo.transform.define;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.ipan.jfinal.tdo.config.Tpl;
import com.ipan.jfinal.tdo.transform.DefineMethodRegister.DefineMethod;
import com.ipan.jfinal.tdo.transform.TransducerUtil;
import com.ipan.kits.text.MoreStringUtil;

/**
 * 发生额出现两列的情况（仅支持文件格式导入）
 * 
 * @author iPan
 * @date 2022-02-09
 */
public class DefineDoubleCellAmount implements DefineMethod {

	@Override
	public String name() {
		return "doubleAmount";
	}

	@Override
	public String call(Map rowMap, Object rowKey, String params, Tpl config) {
		if (StringUtils.isBlank(params)) {
			throw new IllegalArgumentException("参数不能为空！");
		}
		String[] arr = params.split(",");
		if (arr.length != 2) {
			throw new IllegalArgumentException("参数格式不正确，个数必须是2！");
		}
//		Integer ln = Integer.valueOf(arr[0].trim());
//		Integer rn = Integer.valueOf(arr[1].trim());
//		if (ln < 1 || rn < 1) {
//			throw new IllegalArgumentException("参数格式不正确，列数必须大于0！");
//		}
		String leftVal = MoreStringUtil.toStr(rowMap.get(arr[0].trim()));
		String rightVal = MoreStringUtil.toStr(rowMap.get(arr[1].trim()));
		if (StringUtils.isBlank(leftVal) && StringUtils.isBlank(rightVal)) {
			throw new IllegalArgumentException("文件数据不合规，借贷发生额不能都为空！");
		}
		if (StringUtils.isNotBlank(leftVal) && StringUtils.isNotBlank(rightVal)) {
			throw new IllegalArgumentException("文件数据不合规，借贷发生额不能都不为空！");
		}
		
		String je = "";
		if (StringUtils.isBlank(leftVal)) {
			je = TransducerUtil.clearMoney(rightVal);
		} else {
			je = TransducerUtil.clearMoney(leftVal);
		}
		return je;
	}

}
