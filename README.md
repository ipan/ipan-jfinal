# ipan-jfinal

#### 介绍
以JFinal框架做后端架构内核，做了一层包装；包装内容如下：
1）所有业务操作封装Service层，在框架层面固定了；
2）Controller控制器封装了基本增删改查、导入/导出；
3）拦截器封装了日志记录、角色拦截、权限拦截；
4）Handler处理增加了静态资源直接返回，不需要进入框架；
5）用户会话控制、缓存控制与系统解耦，具体控制需要具体系统自己实现接口；
6）导入/导出、权限控制、会话控制都只提供标准接口，需要自行实现；
7）Layui简单封装；
8）模板引擎支持角色、权限的指令；
9）异常处理能够识别是ajax还是html请求，自动做处理；
10）增加图片渲染的处理；
11）多端登录的配置；

#### 软件架构
必须依赖jfinal、ehcache、druid、fastjson、guava、commons-lang3、slf4j、log4j、poi、ipan-kits、ipan-poi3、dom4j、；


#### 安装教程
```dependency
<dependency>
	<groupId>com.ipan</groupId>
	<artifactId>ipan-jfinal</artifactId>
	<version>2.1.0</version>
</dependency>
