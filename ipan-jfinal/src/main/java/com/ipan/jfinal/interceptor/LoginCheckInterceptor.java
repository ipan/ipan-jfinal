package com.ipan.jfinal.interceptor;

import javax.servlet.http.HttpServletRequest;

import com.ipan.jfinal.plugin.userinfo.UserInfo;
import com.ipan.jfinal.plugin.userinfo.UserInfoManager;
import com.ipan.kits.web.ServletUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;

/**
 * 登录拦截器
 * 
 * 针对具体项目需要自行实现，还需要验证是否登录、会话超时、更新最后访问时间等；
 * 
 * 注意：多端拦截器需要自行实现，并且拦截器必须是有默认构造器的，不能带参！
 * 
 * @author iPan
 * @version 2018-04-06
 */
public class LoginCheckInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		Controller con = inv.getController();
		HttpServletRequest httpRequest = con.getRequest();
		UserInfo userinfo = UserInfoManager.getUserInfo(httpRequest, null);
		if (userinfo == null) { // 未登录
			boolean isAjax = ServletUtil.isAjax(httpRequest);
			if (isAjax) {
				Kv param = Kv.by("code", 999).set("msg", "账号未登入"); // 状态码根据具体项目做设置
				con.renderJson(param);
			} else {
				con.redirect("/login");
			}
			return ;
		}
		con.setAttr("_username", userinfo.getUserName());
		con.setAttr("_nickname", userinfo.getNickName());
		con.setAttr("request", con.getRequest()); // request供部分组件使用！
		inv.invoke();
		// 对当前登录用户显示系统提醒，可以在这里写入；
//		if (userinfo != null) {
//			
//		}
	}

}
