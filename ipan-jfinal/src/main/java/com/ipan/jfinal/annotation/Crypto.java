package com.ipan.jfinal.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据库字段脱敏
 * 
 * 标记在类信息上，为了提高性能，不用每个字段都去检查；
 * 
 * @author iPan
 * @date 2021-12-07
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Crypto {
	
	/** 要加密的字段数组 */
	String[] value() default ""; // 注意：是数据库字段名，不是Java字段名；
	
}
